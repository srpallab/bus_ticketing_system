from django import forms
from django.forms import ModelForm
from .models import TicketInfo


class TicketSearchForm(forms.Form):
    """Ticket Search Form for frontpage.

    """
    start_point = forms.CharField(
        label="FROM ", widget=forms.TextInput(attrs={'class': 'autocomplete'}))
    end_point = forms.CharField(
        label="TO ", widget=forms.TextInput(attrs={'class': 'autocomplete'}))
    # doc https://docs.djangoproject.com/en/2.2/ref/forms/widgets/
    date = forms.DateField(widget=forms.DateInput(
        attrs={'class': 'datepicker', 'type': 'date', 'name': 'date_input'}))


class TickeBookingInfoForm(ModelForm):
    """ After booking Seat get info about booking user
    """
    class Meta:
        model = TicketInfo
        fields = ['ticket_holder_name',
                  'ticket_holder_phone', 'ticket_holder_email']
