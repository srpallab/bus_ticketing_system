from django.http import JsonResponse
from django.shortcuts import render, reverse, redirect
from urllib.parse import urlencode
from .forms import TicketSearchForm, TickeBookingInfoForm
from .models import RouteInfo, BusInfo, TicketInfo


def ajax_method(request):
    query_set = RouteInfo.objects.all()
    data = []
    num_for = len(query_set)
    for i in range(0, num_for):
        b = {'start_point': query_set[i].start_point}
        data.append(dict(b))
        # b = {'end_point': query_set[i].end_point}
        # data.append(dict(b))
        # print(b)
        # print(query_set[i].start_point)
    return JsonResponse(data, safe=False)


def index(request):
    context = {
        'form': TicketSearchForm
    }

    if request.GET:
        # print(request.GET.get('start_point'))
        s = request.GET.get('start_point').capitalize()
        e = request.GET.get('end_point').capitalize()
        d = request.GET.get('date')

        # print(s, e, d)
        # doc https://realpython.com/django-redirects/
        base_url = reverse('booking')
        query_string = urlencode({'s': s, 'e': e, 'd': d})
        url = '{}?{}'.format(base_url, query_string)
        # print(url)
        return redirect(url)
    return render(request, 'frontpage/frontpage.html', context)


def booking(request):
    # Taking the valuse from Index View Get Method.
    s = request.GET['s']
    e = request.GET['e']
    d = request.GET['d']

    # Finding all the buses given start point and end point
    query_bus_route_info = RouteInfo.objects.filter(
        start_point__exact=s, end_point__exact=e)

    # Counting how many buses
    bus_count = len(query_bus_route_info)
    # Calculating How much Seats avaible
    # Finding how mamny seats are taken for given date
    seats_taken_list = {}
    # print(seats_taken_list)
    for i in range(0, bus_count):
        # Finding bus tickets and booked seat_numbers
        seats_taken_query_set = TicketInfo.objects.filter(
            journy_date__exact=d,
            bus_name__exact=query_bus_route_info[i].bus_name)
        # Going through all the tickets and adding to list
        for j in seats_taken_query_set:
            if str(query_bus_route_info[i].bus_name) in seats_taken_list:
                seats_taken_list[str(
                    query_bus_route_info[i].bus_name)] += ',' + j.seat_numbers
            else:
                seats_taken_list[str(
                    query_bus_route_info[i].bus_name)] = j.seat_numbers
    # Making A quick bus name list depanding on serarch
    bus_name_list = []
    for i in range(0, bus_count):
        bus_name_list.append(query_bus_route_info[i].bus_name)
    # Posting Data into Database
    if request.POST:
        for i in range(0, bus_count):
            temp_name = str(query_bus_route_info[i].bus_name) + \
                '-'+str(query_bus_route_info[i].fare)
            if request.POST.getlist(temp_name):
                # If any bus seat submitted it will show here
                seats_selected = request.POST.getlist(temp_name)
                total_fare_cal = query_bus_route_info[i].fare * \
                    len(seats_selected)
                print(total_fare_cal)
                # print(list(seats_selected))
                # TODO:  try not to post same seat in same date and same bus
                p = TicketInfo(
                    bus_company_name=query_bus_route_info[i].bus_name.owned_by.company_name,
                    bus_name=query_bus_route_info[i].bus_name,
                    route_name=query_bus_route_info[i].route_name,
                    seat_numbers=seats_selected,
                    total_fare=total_fare_cal,
                    journy_date=d,
                    is_booked=True,)
                p.save()
                request.session['session_ticket_id'] = p.ticket_id
                return redirect('confirmation')

    # print(seats_taken_list)

    context = {
        'bus_route_info': query_bus_route_info,
        'seats_av': seats_taken_list,
        'limit': 4,
        'booked_seat': seats_taken_list,
    }

    return render(request, 'frontpage/booking.html', context)


def confirmation(request):
    if request.session.has_key('session_ticket_id'):
        # print(tick)
        session_ticket_id = request.session['session_ticket_id']
        ticket_find_query = TicketInfo.objects.get(ticket_id=session_ticket_id)
        print(ticket_find_query)
        if request.POST:
            form = TickeBookingInfoForm(request.POST)
            if form.is_valid():
                # print(form.cleaned_data['ticket_holder_name'])
                ticket_find_query.ticket_holder_name = form.cleaned_data['ticket_holder_name']
                ticket_find_query.ticket_holder_phone = form.cleaned_data['ticket_holder_phone']
                ticket_find_query.ticket_holder_email = form.cleaned_data['ticket_holder_email']
                ticket_find_query.save()
                context = {
                    'name': ticket_find_query.ticket_holder_name,
                    'phone': ticket_find_query.ticket_holder_phone,
                    'email': ticket_find_query.ticket_holder_email,
                    'route': ticket_find_query.route_name,
                    'seats': ticket_find_query.seat_numbers,
                    'fare': ticket_find_query.total_fare,
                }
                return render(request, 'frontpage/thank-you.html', context)
    else:
        return render(request, 'frontpage/frontpage.html', {'form': TicketSearchForm})
    context = {
        'form': TickeBookingInfoForm,
    }
    return render(request, 'frontpage/booking-info.html', context)
