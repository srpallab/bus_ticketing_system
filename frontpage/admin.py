from django.contrib import admin
from .models import BusCompany, BusInfo, RouteInfo, CounterInfo, TicketInfo


# admin.site.register(BusCompany)
@admin.register(BusCompany)
class CustomBusCompanyAdd(admin.ModelAdmin):
    fields = ('company_name', 'company_headoffice',)
    list_display = ('company_name', 'company_headoffice',)
    # doc https://code.djangoproject.com/wiki/CookBookNewformsAdminAndUser
    # https://stackoverflow.com/questions/4754485/dry-way-to-add-created-modified-by-and-time
    # overriding save model to take login user as created_by user

    def save_model(self, request, obj, form, change):
        if change:
            obj.updated_by = request.user.id
        else:
            obj.created_by = request.user.id
        obj.save()


@admin.register(BusInfo)
class CustomBusInfo(admin.ModelAdmin):
    fields = ('bus_name', 'bus_serial_number', 'bus_chassis_number',
              'bus_registration_number', 'seats_capacity', 'dirver_name', 'owned_by',)
    list_display = ('bus_name', 'bus_registration_number',
                    'seats_capacity', 'dirver_name', 'owned_by',)

    def save_model(self, request, obj, form, change):
        if change:
            obj.updated_by = request.user.id
        else:
            obj.created_by = request.user.id
        obj.save()


@admin.register(RouteInfo)
class CustomRouteInfo(admin.ModelAdmin):
    fields = ('bus_name', 'route_name', 'start_point',
              'end_point', 'fare', 'dep_time', 'arr_time')
    list_display = ('bus_name', 'route_name', 'fare',)

    def save_model(self, request, obj, form, change):
        if change:
            obj.updated_by = request.user.id
        else:
            obj.created_by = request.user.id
        obj.save()


@admin.register(CounterInfo)
class CustomCounterInfo(admin.ModelAdmin):
    fields = ('counter_name', 'counter_address',)
    list_display = ('counter_name', 'counter_address',)

    def save_model(self, request, obj, form, change):
        if change:
            obj.updated_by = request.user.id
        else:
            obj.created_by = request.user.id
        obj.save()


@admin.register(TicketInfo)
class CustomTicketInfo(admin.ModelAdmin):
    fields = ('ticket_holder_name', 'ticket_holder_phone', 'ticket_holder_email', 'bus_company_name',
              'bus_name', 'route_name', 'counter_name', 'total_fare', 'journy_date', 'seat_numbers', 'is_booked', 'is_confirmed',)
    list_display = ('bus_name', 'route_name', 'journy_date',
                    'seat_numbers', 'is_booked', 'is_confirmed')

    def save_model(self, request, obj, form, change):
        if change:
            obj.updated_by = request.user.id
        else:
            obj.created_by = request.user.id
        obj.save()
