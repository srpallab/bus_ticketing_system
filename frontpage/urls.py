from django.urls import path
from frontpage.views import index, booking, ajax_method, confirmation

urlpatterns = [
    path('', index, name='index'),
    path('ajax_station', ajax_method, name='ajax_method'),
    path('booking', booking, name='booking'),
    path('confirmation', confirmation, name='confirmation'),
]
