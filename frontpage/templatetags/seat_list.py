from django import template

register = template.Library()


@register.filter(name='seats')
def seats(num):
    ascii_to_char = 65
    seat_list = []
    # print(num)
    for i in range(0, num//4):
        for j in range(1, 5):
            seat_list.append(chr(ascii_to_char)+str(j))
        ascii_to_char += 1
    return seat_list


@register.filter(name='booked_or_not')
def booked_or_not(booked, bus):
    return booked[str(bus)]


@register.filter(name='av_count')
def av_count(seats_av, bus):
    # print(seats_av, bus)
    if str(bus) in seats_av:
        seats = seats_av[str(bus)]
        # print(seats.split(','))
        booked_count = len(seats.split(','))
        # print(booked_count)
        return booked_count
    else:
        return 0


@register.filter(name='sub')
def sub(book, total):
    # print(book, total)
    return total - book
