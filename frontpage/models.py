from django.db import models
# from django.utils import timezone
from django.contrib.auth.forms import User


class BusCompany(models.Model):
    """Bus Company Information Table
    """
    company_id = models.AutoField(primary_key=True)
    company_name = models.CharField(max_length=100)
    company_headoffice = models.CharField(max_length=255)
    # doc https://docs.djangoproject.com/en/2.2/ref/models/fields/#datefield
    # auto_now_add is one time only
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField(null=True)
    # auto_now give date when update update
    updated_on = models.DateTimeField(null=True, auto_now=True)

    def __str__(self):
        return self.company_name


class BusInfo(models.Model):
    """ Bus Information Table"""
    bus_id = models.AutoField(primary_key=True)
    bus_name = models.CharField(max_length=30)
    bus_serial_number = models.CharField(max_length=30)
    bus_chassis_number = models.CharField(max_length=30)
    bus_registration_number = models.CharField(max_length=30)
    seats_capacity = models.IntegerField()
    dirver_name = models.ForeignKey(
        User, default=None, on_delete=models.SET_DEFAULT)
    owned_by = models.ForeignKey(
        BusCompany, default=None, on_delete=models.SET_DEFAULT)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField(null=True)
    updated_on = models.DateTimeField(null=True, auto_now=True)

    def __str__(self):
        return self.bus_name


class RouteInfo(models.Model):
    """Table for RouteInfo
    """
    route_id = models.AutoField(primary_key=True)
    bus_name = models.ForeignKey(
        BusInfo, default=None, on_delete=models.SET_DEFAULT)
    route_name = models.CharField(max_length=30)
    start_point = models.CharField(max_length=10)
    end_point = models.CharField(max_length=10)
    dep_time = models.TimeField()
    arr_time = models.TimeField()
    fare = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField(null=True)
    updated_on = models.DateTimeField(null=True, auto_now=True)

    def __str__(self):
        return self.route_name


class CounterInfo(models.Model):
    """Table for counter information
    """
    counter_id = models.AutoField(primary_key=True)
    counter_name = models.CharField(max_length=30)
    counter_address = models.CharField(max_length=130)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField()
    updated_by = models.IntegerField(null=True)
    updated_on = models.DateTimeField(null=True, auto_now=True)

    def __str__(self):
        return self.counter_name


class TicketInfo(models.Model):
    """Table for Ticket information

    """
    ticket_id = models.AutoField(primary_key=True)
    ticket_holder_name = models.CharField(max_length=130, null=True)
    ticket_holder_phone = models.CharField(max_length=11, null=True)
    ticket_holder_email = models.EmailField(null=True)
    # bus_company_name = models.ForeignKey(
    #    BusCompany, default=None, on_delete=models.SET_DEFAULT)
    # bus_id = models.ForeignKey(
    #    BusInfo, default=None, on_delete=models.SET_DEFAULT)
    # route_id = models.ForeignKey(
    #    RouteInfo, default=None, on_delete=models.SET_DEFAULT)
    # counter_id = models.ForeignKey(
    #    CounterInfo, default=None, on_delete=models.SET_DEFAULT, null=True)
    bus_company_name = models.CharField(max_length=130)
    bus_name = models.CharField(max_length=130)
    route_name = models.CharField(max_length=130)
    counter_name = models.CharField(max_length=130, null=True)
    seat_numbers = models.CharField(max_length=12)
    total_fare = models.IntegerField()
    journy_date = models.DateField()
    is_booked = models.BooleanField(default=False)
    is_confirmed = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(null=True)
    updated_by = models.IntegerField(null=True)
    updated_on = models.DateTimeField(null=True, auto_now=True)
